module.exports = function () {
    var Widget = this.Widget;

    this.Given(/^I visit products page$/, function () {
        return this.driver.visit('http://localhost:3002/#products');
    });

    this.When(/^I click on the button "add" of (\d+) product$/, function (product) {
        return new Widget.ProductsList().clickButton(+product);
    });

    this.Then(/^I see (\d+) products in the cart$/, function(number){
        return new Widget.ProductsCart().getTitle().should.eventually.eql('Cart: ' + number);
    });

    this.When(/^I click on the cart link$/, function(){
        return new Widget.ProductsCart().goToCart();
    });

    this.Then(/^I am on the cart page$/, function(){
        return this.driver.getCurrentUrl().should.eventually.eql('http://localhost:3002/#cart');
    });

    this.Then(/^I change the count of (\d+) product to (\d+)$/, function(elem, count){
        return new Widget.QuantityList().inputQuantity(+elem, count);
    });

    this.When(/^I click the recalculate button and total sum have been recalculated$/, function(){
        var prev = new Widget.Sum().total();
        return new Widget.Recalculate().recalculate().then(function(){
            return new Widget.Sum().total().should.not.be.eql(prev);
        });
    });

    this.When(/^I input in (\d+) field email "([^"]*)"$/, function(elem, value){
        return new Widget.FormFields().inputValue(+elem, value);
    });

    this.When(/^I input in (\d+) field phone "([^"]*)"$/, function(elem, value){
        return new Widget.FormFields().inputValue(+elem, value);
    });

    this.When(/^I click the button "make order"$/, function(){
        return new Widget.MakeOrder().order();
    });

    this.Then(/^I see that my order is successfully processed$/, function(){
        return this.driver.getCurrentUrl().then(function(addr){
            var hash = addr.split('/');
            return addr == 'http://localhost:3002/#order/'+hash[hash.length-1];
        });
    });

};