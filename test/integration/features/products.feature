Feature: Products Feature

  Background:
    Given I visit products page

  Scenario: Viewing products
    Then I should see "5" products on the page

  Scenario: Determining available/unavailable products
    Then I should see product "0" is "available"
    Then I should see product "1" is "available"
    Then I should see product "2" is "not available"
    Then I should see product "3" is "available"
    Then I should see product "4" is "not available"

  Scenario: I should nod add unavailable product to the cart
    When I click on product "2"
    Then I see cart is empty

  Scenario: I can add available product to the cart
    When I click on product "0"
    And I click on product "1"
    Then I see "2" products in the cart
    When I click on product "0"
    Then I see "1" products in the cart
    When I click on product "3"
    Then I see "2" products in the cart
    When I click on product "1"
    Then I see "1" products in the cart