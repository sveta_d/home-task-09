Feature: Shopping Feature

    Scenario: Visit product page and order 2 products
      Given I visit products page
      When I click on the button "add" of 0 product
        And I click on the button "add" of 1 product
      Then I see 2 products in the cart
      When I click on the cart link
      Then I am on the cart page
      When I change the count of 0 product to 3
        And I change the count of 1 product to 2
        And I click the recalculate button and total sum have been recalculated
        And I input in 0 field email "sveta@mail.ua"
        And I input in 1 field phone "9999555"
        And I click the button "make order"
      Then I see that my order is successfully processed
