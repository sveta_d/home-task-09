module.exports = function(){
    var Widget = this.Widget;

    Widget.ProductItem = Widget.extend({
        clickButton: function(){
            return this.click('button');
        }
    });

    Widget.ProductsList = Widget.List.extend({
        root: 'table.products',
        itemSelector: 'tr.product',
        itemClass: Widget.ProductItem,
        clickButton : function(param){
            return this.at(param).then(function (widget) {
                return widget.clickButton();
            });
        }
    });

    Widget.ProductsCart = Widget.extend({
        root: '.cart-strip',
        getTitle: function () {
            return this.read('a');
        },
        goToCart: function(){
            return this.click('a');
        }
    });

    Widget.Quantity = Widget.extend({
        inputQuantity: function(number){
            return this.fill(number);
        }
    });

    Widget.QuantityList = Widget.List.extend({
        root: 'table.cart',
        itemSelector: 'tr.product input',
        itemClass: Widget.Quantity,
        inputQuantity: function(elem, number){
            return this.at(elem).then(function (widget) {
                return widget.inputQuantity(number);
            });
        }
    });

    Widget.Recalculate = Widget.extend({
        root: 'table.cart .buttons',
        recalculate: function(){
            return this.click('button[data-action="recalculate"]');
        }
    });

    Widget.Sum = Widget.extend({
        root: '.cart tr.total',
        itemSelector: 'td:nth-child(1)',
        total: function(){
           return this.getText();
        }
    });

    Widget.FormField = Widget.extend({
        inputValue: function(value){
            return this.fill(value);
        }
    });

    Widget.FormFields = Widget.List.extend({
        root: 'form',
        itemSelector: 'input',
        itemClass: Widget.FormField,
        inputValue: function(elem, value){
            return this.at(elem).then(function (widget) {
                return widget.inputValue(value);
            });
        }
    });

    Widget.MakeOrder = Widget.extend({
        root: 'table.cart .buttons',
        order: function(){
            return this.click('button[data-action="order"]');
        }
    });
};