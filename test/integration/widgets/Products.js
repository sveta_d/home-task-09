module.exports = function () {
    var Widget = this.Widget;

    Widget.ProductsCart = Widget.extend({
        root: '.cart-strip',
        getTitle: function () {
            return this.read('a');
        }
    });

    Widget.ProductsItem = Widget.extend({
        isAvailable: function () {
            return this.find('button').then(function (button) {
                return button.getAttribute('disabled').then(function (disabled) {
                    return !disabled;
                })
            });
        },
        clickTheButton: function () {
            return this.click('button');
        }
    });

    Widget.ProductsList = Widget.List.extend({
        root: '.products table.products',
        itemSelector: 'tr.product',
        itemClass: Widget.ProductsItem,
        isAvailable: function (position) {
            return this.at(position).then(function (widget) {
                return widget.isAvailable();
            });
        },
        clickTheButton: function (position) {
            return this.at(position).then(function (widget) {
                return widget.clickTheButton();
            });
        }
    });
};
