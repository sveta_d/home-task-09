var express = require('express');
var router = express.Router();
var Order = require('../mongo/Order');

router.get('/:hash', function (req, res) {
    Order
        .findOne({hash: req.params.hash})
        .populate('items.product')
        .exec(function (err, order) {
            if (err) {
                console.error(err);
                return res.sendStatus(500);
            }
            res.json(order);
        });
});

module.exports = router;