define([
    'backbone',
    'backbone.marionette',
    './template',
    './Service',
    './Model'
], function (Backbone, Marionette, template, CartService, Model) {
    return Marionette.ItemView.extend({
        template: template,

        ui: {
            input: 'input[data-calc=item]',
            email: 'input[data-type=email]',
            phone: 'input[data-type=phone]'
        },

        events: {
            'click button[data-action=recalculate]': 'recalculate',
            'click button[data-action=order]': 'order'
        },

        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },

        _updateCart: function () {
            this.ui.input.each(function () {
                CartService.add(this.dataset.product, parseInt(this.value) || 0);
            });
        },

        recalculate: function () {
            this._updateCart();
            this.model.fetch({
                data: {
                    cart: CartService.serialize()
                }
            })
        },

        order: function () {
            var email = this.ui.email.val();
            var phone = this.ui.phone.val();

            if(!email || !phone) {
                this.ui.email.css('outline', '2px solid #D01818');
                this.ui.phone.css('outline', '2px solid #D01818');
            }

            if (email && phone) {
                this.ui.email.css('outline', '2px solid transparent');
                this.ui.phone.css('outline', '2px solid transparent');

                    this._updateCart();
                    new Model({
                        email: email,
                        phone: phone,
                        items: CartService.serialize()
                    })
                        .save()
                        .done(function (data) {
                            CartService.removeAll();
                            Backbone.history.navigate('order/' + data.hash, {trigger: true, replace: true});
                        });
                }
            }
    });
});