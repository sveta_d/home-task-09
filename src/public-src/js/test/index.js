require.config({
    baseUrl: './js/',
    paths: {
        'jquery': './libs/jquery',
        'mocha': './libs/mocha',
        'chai': './libs/chai'
    }
});

require([
    'mocha',
    'jquery',
    'chai'
], function (mocha, jquery, chai) {
    window.expect = chai.expect;
    window.mocha.setup('bdd');

    require([
        'js/test/specs/sort.spec.js'
    ], function () {
        window.mocha.run();
    });

});