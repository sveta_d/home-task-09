var mongoose = require('./mongoose');
var shortid = require('shortid');

var schema = mongoose.Schema({
    hash: {type: String, default: shortid.generate},
    email: String,
    phone: String,
    items: [{
        quantity: Number,
        product: {type: mongoose.Schema.Types.ObjectId, ref: 'Product'}
    }]
}, {
    collection: 'order'
});

var Order = mongoose.model('Order', schema);

module.exports = Order;